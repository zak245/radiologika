'use strict';

angular.module('radioApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth) {
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    },
    {
      'title': 'Vacations',
      'link': '/session'
    },
    {
      'title': 'Examens',
      'link': '/exam'
    },
    {
      'title': 'Vacation en cour',
      'link': '/session/current'
    }];

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.isActive = function(route) {
      return route === $location.path();
    };

    $scope.CurrentLocation=$location.path();
    //alert($scope.CurrentLocation)
  })
  .controller('SessionDataCtrl', function ($scope, $http, $location, Auth, SweetAlert) {
    $scope.sessionID=localStorage.sessionID;
    $scope.sessionDate=localStorage.sessionDate;
    $scope.sessionRoom=localStorage.sessionRoom;
    $scope.sessionMethod=localStorage.sessionMethod;
    $scope.sessionHead=localStorage.sessionHead;
    $scope.sessionTeam=localStorage.sessionTeam;
    $scope.sessionStatus=localStorage.sessionStatus;

    //close current session
    $scope.closeSession=function(){
        var data={
        id: $scope.sessionID,
        status: "fermer"
      };
      $http.put('/api/sessions/'+ $scope.sessionID, data).
        success(function(data, status, headers, config) {
          localStorage.sessionID=undefined;
          localStorage.sessionDate=undefined;
          localStorage.sessionRoom=undefined;
          localStorage.sessionMethod=undefined;
          localStorage.sessionHead=undefined;
          localStorage.sessionTeam=undefined;
          localStorage.sessionStatus=undefined;

          $scope.sessionID=localStorage.sessionID;
          $scope.sessionDate=localStorage.sessionDate;
          $scope.sessionRoom=localStorage.sessionRoom;
          $scope.sessionMethod=localStorage.sessionMethod;
          $scope.sessionHead=localStorage.sessionHead;
          $scope.sessionTeam=localStorage.sessionTeam;
          $scope.sessionStatus=localStorage.sessionStatus;
          console.log("session closed")
          SweetAlert.swal("Vacation fermer avec success", "", "success");
          $location.path('/session')
        }).
        error(function(data, status, headers, config) {
          // called asynchronously if an error occurs
          // or server returns response with an error status.
       });
    }

  });




