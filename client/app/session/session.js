'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('session', {
        url: '/session',
        templateUrl: 'app/session/session.html',
        controller: 'SessionCtrl'
      })
      .state('currentSession', {
        url: '/session/current',
        templateUrl: 'app/session/current.session.html',
        controller: 'CurrentSessionCtrl'
      })
      .state('newSession', {
        url: '/session/new',
        templateUrl: 'app/session/new.session.html',
        controller: 'NewSessionCtrl'
      });
  });