'use strict';

angular.module('radioApp')
  .controller('SessionCtrl', function ($scope,$http,$location) {
    $scope.sessions={};

    //retrive the sessions list
    $http.get('/api/sessions').
		  success(function(data, status, headers, config) {
		    $scope.sessions=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
	});
  })
  .controller('NewSessionCtrl', function ($scope,$http,$location,SweetAlert) {
    //pre fill the date
    var today = new Date();
    today.setHours(0,0,0,0);
    console.log(today);
	$scope.date=today;

	//get the doctors list
    $scope.doctors={};
	    $http.get('/api/doctors').
		  success(function(data, status, headers, config) {
		    $scope.doctors=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	$scope.getRooms= function(){
	    $scope.rooms={};
	    $http.get('/api/rooms/method/' + $scope.method).
		  success(function(data, status, headers, config) {
		    $scope.rooms=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	    }
	//get the rooms list
	

	//get the methods list
	$scope.methods={};
	    $http.get('/api/methods').
		  success(function(data, status, headers, config) {
		    $scope.methods=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });


	// start the post process
		$scope.session={};
		$scope.sessionTeam=[];

	    $scope.addMember= function(){
	    	$scope.sessionTeam.push($scope.teamMember);
	    }
	    $scope.newSession= function(){
	    	//remove time from the date value
	    	//$scope.date
	    	$scope.session={
	    		date: $scope.date,
	    		room: $scope.room,
	    		method: $scope.method,
	    		sessionHead: $scope.sessionHead,
	    		sessionTeam: $scope.sessionTeam,
	    		status:"new"
	    	};
	    	$http.post('/api/sessions', $scope.session).
			success(function(data, status, headers, config) {
				if(data.errorCode){//session already created with the following data
					SweetAlert.swal({
					   title: "Session Existante",
					   text: "La vacation que vous voulais cree existe deja ",
					   type: "warning",
					   showCancelButton: true,
					   confirmButtonColor: "#6AB821",
					   confirmButtonText: "Ouvrire la Vacation",
					   closeOnConfirm: false,
					   closeOnCancel: true}, 
					function(isConfirm){ 
						if (isConfirm) {
							localStorage.sessionID=data.sessionData._id;
						    localStorage.sessionDate=data.sessionData.date;
						    localStorage.sessionRoom=data.sessionData.room;
						    localStorage.sessionMethod=data.sessionData.method;
						    localStorage.sessionHead=data.sessionData.sessionHead;
						    localStorage.sessionTeam=JSON.stringify(data.sessionData.sessionTeam);
						    console.log(data.sessionData.sessionTeam)
						    localStorage.sessionStatus=data.sessionData.status;
						   	SweetAlert.swal("vacation Du" + $scope.date + "Ouverte");
						   	$location.path('/session')
						}
					});
				}
				else{//session created with success
					console.log(data);
				    localStorage.sessionID=data._id;
				    localStorage.sessionDate=data.date;
				    localStorage.sessionRoom=data.room;
				    localStorage.sessionMethod=data.method;
				    localStorage.sessionHead=data.sessionHead;
				    localStorage.sessionTeam=JSON.stringify(data.sessionTeam);
				    console.log(data.sessionTeam)
				    localStorage.sessionStatus=data.status;
				    $location.path('/session')
				}
			    
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			      console.log($scope.session)
			});
	    }	
    

  })
.controller('CurrentSessionCtrl', function ($scope,$http,$location) {
	$scope.exams = {};

    //retrive the doctors list
	    $http.get('/api/exams/session/'+ localStorage.sessionID).
			  success(function(data, status, headers, config) {
			    $scope.exams=data;
			    console.log(data)
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});
 });
