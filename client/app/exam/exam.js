'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('exam', {
        url: '/exam',
        templateUrl: 'app/exam/exam.html',
        controller: 'ExamCtrl'
      })
      .state('newExam', {
        url: '/exam/new',
        templateUrl: 'app/exam/new.exam.html',
        controller: 'NewExamCtrl'
      })
      .state('analyzeExam', {
        url: '/exam/analyze',
        templateUrl: 'app/exam/analyze.exam.html',
        controller: 'AnalyzeExamCtrl'
      })
      .state('detailsExam', {
        url: '/exam/details/{id}',
        templateUrl: 'app/exam/details.exam.html',
        controller: 'DetailsExamCtrl'
      })
      .state('editExam', {
        url: '/exam/edit/{id}',
        templateUrl: 'app/exam/edit.exam.html',
        controller: 'EditExamCtrl'
      })
      .state('printExam', {
        url: '/exam/print/{id}',
        templateUrl: 'app/exam/print.exam.html',
        controller: 'PrintExamCtrl'
      });
  });