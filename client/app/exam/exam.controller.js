'use strict';

angular.module('radioApp')
  .controller('ExamCtrl', function ($scope,$http,$location) {
    $scope.exams = {};

    //retrive the doctors list
	    $http.get('/api/exams/compact').
			  success(function(data, status, headers, config) {
			    $scope.exams=data;
			    console.log(data)
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});
  })
  .controller('NewExamCtrl', function ($scope,$http,$location,$window) {
	    $scope.exam = {} ;



		 //retrive the examType when the method is selected
		 $scope.method=localStorage.sessionMethod;
		
		 	console.log($scope.method)

		 	$scope.types={};
		    $http.get('/api/exam_types/filterByMethod',
		    {
		        params: {
		            method: $scope.method,
		        }
	    	}).
			  success(function(data, status, headers, config) {
			    $scope.types=data;
			    console.log(data)
			  }).
			  error(function(data, status, headers, config) {
			 });
		

		 //post the new exam to the back end
		 $scope.newExam= function(){
		 	var today = new Date();
	    	$scope.exam={
	    		session: localStorage.sessionID,
	    		date: today,
	    		fname: $scope.fname,
	    		lname: $scope.lname,
	    		age: $scope.age,
	    		gender: $scope.gender,
	    		hospital: $scope.hospital,
	    		service: $scope.service,
	    		reason: $scope.reason,
	    		method: $scope.method,
	    		type: $scope.type,
	    		created_at: today,
	    		status: "non interprete"

	    		
	    	};
	    	$http.post('/api/exams', $scope.exam).
			success(function(data, status, headers, config) {
			    console.log(data)
			    localStorage.examID=data._id;
			    $location.path('/exam/analyze')
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			      console.log($scope.exam)
			});
	    }
	    //post and continue with a new exaxm
	    $scope.newExamReload= function(){
	    			 	var today = new Date();
	    	$scope.exam={
	    		session: localStorage.sessionID,
	    		date: today,
	    		fname: $scope.fname,
	    		lname: $scope.lname,
	    		age: $scope.age,
	    		gender: $scope.gender,
	    		hospital: $scope.hospital,
	    		service: $scope.service,
	    		reason: $scope.reason,
	    		method: $scope.method,
	    		type: $scope.type,
	    		created_at: today,
	    		status: "non interprete"

	    		
	    	};
	    	$http.post('/api/exams', $scope.exam).
			success(function(data, status, headers, config) {
			    console.log($scope.exam)
			    localStorage.examID=data._id;
			    $window.location.reload();
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			      console.log($scope.exam)
			});

	    };

  })
  .controller('AnalyzeExamCtrl', function ($scope,$http,$location) {
    //initialise the text editor
  	$('#report').summernote();
  	$('#conclusion').summernote();
  	console.log(localStorage.sessionTeam)
  	//retrive the team details from the session
  	$scope.sessionTeam=JSON.parse(localStorage.sessionTeam);
  	console.log($scope.sessionTeam)
  	//get the doctors list
    $scope.doctors={};
    $http.get('/api/doctors').
	  success(function(data, status, headers, config) {
	    $scope.doctors=data;
	    console.log(data)
	  }).
	  error(function(data, status, headers, config) {
	    // called asynchronously if an error occurs
	    // or server returns response with an error status.
	 });
	  //get the exam details including the report and conclusion
	 $scope.exam={};
    $http.get('/api/exams/'+ localStorage.examID).
	  success(function(data, status, headers, config) {
	    $scope.exam=data;
	    $('#report').code($scope.exam.report);
	    $('#conclusion').code($scope.exam.conclusion);
	    
	  }).
	  error(function(data, status, headers, config) {
	    // called asynchronously if an error occurs
	    // or server returns response with an error status.
	 });

	  //actions start here
	  //save and go back to a new exam
	$scope.saveChanges=function(){
		var report=$('#report').code();
		var conclusion= $('#conclusion').code();
		var data={
			id: $scope.exam._id,
			interpretedBy:$scope.interpretedBy,
			report: report,
			conclusion: conclusion,
			status: "en cours d\'interpretation"
		};
		$http.put('/api/exams/'+ localStorage.examID,data).
		  success(function(data, status, headers, config) {
		    $scope.exam=data;
		    console.log(data)
		    $location.path('/session/current')
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	};

	 //save final changes and go back to a new exam
	$scope.validateExam=function(){
		var report=$('#report').code();
		var conclusion= $('#conclusion').code();
		var data={
			id: $scope.exam._id,
			interpretedBy:$scope.interpretedBy,
			report: report,
			conclusion: conclusion,
			status: "interprete"
		};
		$http.put('/api/exams/'+ localStorage.examID,data).
		  success(function(data, status, headers, config) {
		    $scope.exam=data;
		    console.log(data)
		    $location.path('/session/current')
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	};

 //save and close with final changes and go back to a new exam
	$scope.closeExam=function(){
		var report=$('#report').code();
		var conclusion= $('#conclusion').code();
		var data={
			id: $scope.exam._id,
			interpretedBy:$scope.interpretedBy,
			report: report,
			conclusion: conclusion,
			status: "libere"
		};
		$http.put('/api/exams/'+ localStorage.examID,data).
		  success(function(data, status, headers, config) {
		    $scope.exam=data;
		    console.log(data)
		    $location.path('/session/current')
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	};



  })
.controller('DetailsExamCtrl', function ($scope,$http,$location,$stateParams,$window) {
	$scope.exam = {};
	//alert($stateParams);

    //retrive the doctors list
	    $http.get('/api/exams/'+ $stateParams.id).
			  success(function(data, status, headers, config) {
			  
			    $scope.exam=data;
			    console.log( $scope.exam)
			  	

			    $("#report").html($scope.exam.report);
			    $("#conclusion").html($scope.exam.conclusion);
			    
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});

	$scope.print=function(){
		    console.log("data")

		$window.open('http://localhost:1337');
	};
})
.controller('EditExamCtrl', function ($scope,$http,$location,$stateParams,$window) {
	$scope.exam = {};
	$('#report').summernote();
  	$('#conclusion').summernote();

	//retrive the doctors list
	    $http.get('/api/exams/'+ $stateParams.id).
			  success(function(data, status, headers, config) {
			  
			    $scope.exam=data;
			    console.log( $scope.exam)
			  	

			    $("#report").code($scope.exam.report);
			    $("#conclusion").code($scope.exam.conclusion);
			    
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});

	 //save and go back to a new exam
	$scope.saveChanges=function(){
		var report=$('#report').code();
		var conclusion= $('#conclusion').code();
		var data={
			id: $scope.exam._id,
			interpretedBy:$scope.interpretedBy,
			report: report,
			conclusion: conclusion,
			status: "en cours d\'interpretation"
		};
		$http.put('/api/exams/'+ localStorage.examID,data).
		  success(function(data, status, headers, config) {
		    $scope.exam=data;
		    console.log(data)
		    $location.path('/session/current')
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	};

	 //save final changes and go back to a new exam
	$scope.validateExam=function(){
		var report=$('#report').code();
		var conclusion= $('#conclusion').code();
		var data={
			id: $scope.exam._id,
			interpretedBy:$scope.interpretedBy,
			report: report,
			conclusion: conclusion,
			status: "interprete"
		};
		$http.put('/api/exams/'+ localStorage.examID,data).
		  success(function(data, status, headers, config) {
		    $scope.exam=data;
		    console.log(data)
		    $location.path('/session/current')
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	};

 //save and close with final changes and go back to a new exam
	$scope.closeExam=function(){
		var report=$('#report').code();
		var conclusion= $('#conclusion').code();
		var data={
			id: $scope.exam._id,
			interpretedBy:$scope.interpretedBy,
			report: report,
			conclusion: conclusion,
			status: "libere"
		};
		$http.put('/api/exams/'+ localStorage.examID,data).
		  success(function(data, status, headers, config) {
		    $scope.exam=data;
		    console.log(data)
		    $location.path('/session/current')
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	};

	$scope.print=function(){
		    console.log("data")

		$window.open('http://localhost:1337');
	};
})
.controller('PrintExamCtrl', function ($scope,$http,$location,$stateParams,$window) {
	$scope.exam = {};
	//alert($stateParams);

    //retrive the doctors list
	    $http.get('/api/exams/'+ $stateParams.id).
			  success(function(data, status, headers, config) {
			  
			    $scope.exam=data;
			    console.log( $scope.exam)
			  	

			    $("#report").html($scope.exam.report);
			    $("#conclusion").html($scope.exam.conclusion);
			    
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});
			 
			  
			  	
			  	
			  
			  

	
});
