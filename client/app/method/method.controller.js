'use strict';

angular.module('radioApp')
  .controller('MethodCtrl', function ($scope,$http,$location) {
    $scope.methods={};
	    $http.get('/api/methods').
		  success(function(data, status, headers, config) {
		    $scope.methods=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
  })
  .controller('NewMethodCtrl', function ($scope,$http,$location) {
    $scope.method={};
	    $scope.name="";
	    $scope.info="";

	    $scope.newMethod= function(){
	    	$scope.method={
	    		name: $scope.name,
	    		info: $scope.info,
	    		active: true
	    	};
	    	$http.post('/api/methods', $scope.method).
			success(function(data, status, headers, config) {
			    console.log($scope.method)
			    $location.path('/method')
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			});
	    }
  });
