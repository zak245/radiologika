'use strict';

describe('Controller: MethodCtrl', function () {

  // load the controller's module
  beforeEach(module('radioApp'));

  var MethodCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MethodCtrl = $controller('MethodCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
