'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('method', {
        url: '/method',
        templateUrl: 'app/method/method.html',
        controller: 'MethodCtrl'
      })
      .state('newMethod', {
        url: '/method/new',
        templateUrl: 'app/method/new.method.html',
        controller: 'NewMethodCtrl'
      });
  });