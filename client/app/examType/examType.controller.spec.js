'use strict';

describe('Controller: ExamTypeCtrl', function () {

  // load the controller's module
  beforeEach(module('radioApp'));

  var ExamTypeCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExamTypeCtrl = $controller('ExamTypeCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
