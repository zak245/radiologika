'use strict';

angular.module('radioApp')
  .controller('ExamTypeCtrl', function ($scope,$http,$location) {
    $scope.examTypes = {};

	    //retrive the doctors list
	    $http.get('/api/exam_types').
			  success(function(data, status, headers, config) {
			    $scope.examTypes=data;
			    console.log(data)
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});

  })
  .controller('NewExamTypeCtrl', function ($scope,$http,$location) {
  	//initialise the text editor
  	$('#reportModel').summernote();
  	$('#techModel').summernote();
  	$('#conclusionModel').summernote();
    
    //retrive the ranks for the select box
    $scope.methods={};
	    $http.get('/api/methods').
		  success(function(data, status, headers, config) {
		    $scope.methods=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });

	// start the post process
		$scope.examType={};
	    $scope.name="";
	    $scope.method="";
	    $scope.reportModel="";
	    $scope.techModel="";
	    $scope.conclusionModel="";

	    $scope.getRank= function(){
	    	$scope.reportModel = $('#reportModel').code();
	    	$('#conclusionModel').code($scope.reportModel);

	    	console.log($scope.reportModel)
	    }
	    $scope.newExamType= function(){
	    	$scope.reportModel = $('#reportModel').code();
	    	$scope.techModel = $('#techModel').code();
	    	$scope.conclusionModel = $('#conclusionModel').code();
	    	$scope.examType={
	    		name: $scope.name,
	    		method: $scope.method,
	    		techModel: $scope.techModel,
	    		reportModel: $scope.reportModel,
	    		conclusionModel: $scope.conclusionModel,
	    		active: true
	    	};
	    	console.log($scope.rank)
	    	$http.post('/api/exam_types', $scope.examType).
			success(function(data, status, headers, config) {
			    console.log($scope.doctor)
			    $location.path('/examType')
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			      console.log($scope.doctor)
			});
	    }	
  });
