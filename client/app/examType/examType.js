'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('examType', {
        url: '/examType',
        templateUrl: 'app/examType/examType.html',
        controller: 'ExamTypeCtrl'
      })
      .state('newExamType', {
        url: '/examType/new',
        templateUrl: 'app/examType/new.examType.html',
        controller: 'NewExamTypeCtrl'
      });
  });