'use strict';

angular.module('radioApp')
  .controller('EquipmentCtrl', function ($scope,$http,$location) {
    $scope.equipments = {};

    //retrive the equipments list
	    $http.get('/api/equipments').
			  success(function(data, status, headers, config) {
			    $scope.equipments=data;
			    console.log(data)
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});
  })
  .controller('NewEquipmentCtrl', function ($scope,$http,$location) {
	    $scope.equipment = {};

	    $scope.newEquipment= function(){
	    	$scope.equipment={
	    		brand: $scope.brand,
	    		name: $scope.name,
	    		year: $scope.year
	    	};

	    	$http.post('/api/equipments', $scope.equipment).
			success(function(data, status, headers, config) {
			    console.log($scope.equipment)
			    $location.path('/equipment')
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			      console.log($scope.equipment)
			});
	    }	

  });
