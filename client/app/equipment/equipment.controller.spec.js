'use strict';

describe('Controller: EquipmentCtrl', function () {

  // load the controller's module
  beforeEach(module('radioApp'));

  var EquipmentCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EquipmentCtrl = $controller('EquipmentCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
