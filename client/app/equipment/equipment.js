'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('equipment', {
        url: '/equipment',
        templateUrl: 'app/equipment/equipment.html',
        controller: 'EquipmentCtrl'
      })
      .state('newEquipment', {
        url: '/equipment/new',
        templateUrl: 'app/equipment/new.equipment.html',
        controller: 'NewEquipmentCtrl'
      });
  });