'use strict';

angular.module('radioApp')
  .controller('RoomCtrl', function ($scope,$http,$location) {
     $scope.rooms = {};

	    //retrive the rooms list
	    $http.get('/api/rooms').
			  success(function(data, status, headers, config) {
			    $scope.rooms=data;
			    console.log(data)
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});
  })
  .controller('NewRoomCtrl', function ($scope,$http,$location) {
    $scope.equipments={};
	    $http.get('/api/equipments').
		  success(function(data, status, headers, config) {
		    $scope.equipments=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });

	$scope.methods={};
	    $http.get('/api/methods').
		  success(function(data, status, headers, config) {
		    $scope.methods=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });

	// start the post process
		$scope.room={};
	    $scope.name="";
	    $scope.equipment="";

	    $scope.newRoom= function(){
	    	$scope.doctor={
	    		name: $scope.name,
	    		equipment: $scope.equipment,
	    		active: true
	    	};

	    	$http.post('/api/rooms', $scope.doctor).
			success(function(data, status, headers, config) {
			    console.log($scope.room)
			    $location.path('/room')
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			      console.log($scope.room)
			});
	    }
  });
