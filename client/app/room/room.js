'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('room', {
        url: '/room',
        templateUrl: 'app/room/room.html',
        controller: 'RoomCtrl'
      })
      .state('newRoom', {
        url: '/room/new',
        templateUrl: 'app/room/new.room.html',
        controller: 'NewRoomCtrl'
      });
  });