'use strict';

angular.module('radioApp')
  .controller('MainCtrl', function ($scope, $http) {
    $scope.awesomeThings = [];

    $http.get('/api/things').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
    });

    $scope.addThing = function() {
      if($scope.newThing === '') {
        return;
      }
      $http.post('/api/things', { name: $scope.newThing });
      $scope.newThing = '';
    };

    $scope.deleteThing = function(thing) {
      $http.delete('/api/things/' + thing._id);
    };

    $scope.sessionID=localStorage.sessionID;
    $scope.sessionDate=localStorage.sessionDate;
    $scope.sessionRoom=localStorage.sessionRoom;
    $scope.sessionMethod=localStorage.sessionmethod;
    $scope.sessionHead=localStorage.sessionHead;
    $scope.sessionTeam=localStorage.sessionTeam;
    $scope.sessionStatus=localStorage.sessionStatus;

  });
