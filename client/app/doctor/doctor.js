'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('doctor', {
        url: '/doctor',
        templateUrl: 'app/doctor/doctor.html',
        controller: 'DoctorCtrl'
      })
      .state('doctorNew', {
        url: '/doctor/new',
        templateUrl: 'app/doctor/doctor.create.html',
        controller: 'DoctorCreateCtrl'
      });
  });