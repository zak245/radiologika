'use strict';

angular.module('radioApp')
  .controller('DoctorCtrl', function ($scope,$http,$location) {
	    $scope.doctors = {};

	    //retrive the doctors list
	    $http.get('/api/doctors').
			  success(function(data, status, headers, config) {
			    $scope.doctors=data;
			    console.log(data)
			  }).
			  error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
		});

  })
  .controller('DoctorCreateCtrl', function ($scope,$http,$location) {
    //retrive the ranks for the select box
    $scope.ranks={};
	    $http.get('/api/ranks/name').
		  success(function(data, status, headers, config) {
		    $scope.ranks=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });

	// start the post process
		$scope.doctor={};
	    $scope.name="";
	    $scope.rank="";

	    $scope.getRank= function(){
	    	console.log($scope.rank)
	    }
	    $scope.newDoctor= function(){
	    	$scope.doctor={
	    		name: $scope.name,
	    		rank: $scope.rank,
	    		active: true
	    	};
	    	console.log($scope.rank)
	    	$http.post('/api/doctors', $scope.doctor).
			success(function(data, status, headers, config) {
			    console.log($scope.doctor)
			    $location.path('/doctor')
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			      console.log($scope.doctor)
			});
	    }	
    
  });
