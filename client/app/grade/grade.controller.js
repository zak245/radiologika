	'use strict';

	angular.module('radioApp')
	  .controller('GradeCreateCtrl', function ($scope,$http,$location) {
	    $scope.rank={};
	    $scope.name="";
	    $scope.info="";

	    $scope.newRank= function(){
	    	$scope.rank={
	    		name: $scope.name,
	    		info: $scope.info,
	    		active: true
	    	};
	    	$http.post('/api/ranks', $scope.rank).
			success(function(data, status, headers, config) {
			    console.log($scope.rank)
			    $location.path('/grade')
			  }).
			error(function(data, status, headers, config) {
			    // called asynchronously if an error occurs
			    // or server returns response with an error status.
			});
	    }
	    
	})
	  .controller('GradeCtrl', function ($scope,$http,$location) {
	   
	    $scope.ranks={};
	    $http.get('/api/ranks').
		  success(function(data, status, headers, config) {
		    $scope.ranks=data;
		    console.log(data)
		  }).
		  error(function(data, status, headers, config) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		 });
	    
	});
