'use strict';

angular.module('radioApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('grade', {
        url: '/grade',
        templateUrl: 'app/grade/grade.html',
        controller: 'GradeCtrl'
      })
      .state('gradeNew', {
        url: '/grade/new',
        templateUrl: 'app/grade/grade.create.html',
        controller: 'GradeCreateCtrl'
      });
  });