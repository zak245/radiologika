'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ExamSchema = new Schema({
  session: String,
  date:Date,
  fname: String,
  lname: String,
  age: Number,
  gender: String,
  service: String,
  hospital: String,
  reason: String,
  method: String,
  type: String,
  tech: String,
  report: String,
  conclusion: String,
  interpretedBy: String,
  team:[{
  	name: String
  }],
  created_at: Date,
  updated_at: Date,
  status: String


});

module.exports = mongoose.model('Exam', ExamSchema);