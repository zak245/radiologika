'use strict';

var _ = require('lodash');
var Exam = require('./exam.model');
var ExamType = require('../examType/examType.model');
var jsreport = require('jsreport');
var http = require('http');


// Get list of exams
exports.index = function(req, res) {
  Exam.find(function (err, exams) {
    if(err) { return handleError(res, err); }
    return res.json(200, exams);
  });
};

// Get compact list of exams(not all fields)
exports.compactList = function(req, res) {
  Exam.find(function (err, exams) {
    if(err) { return handleError(res, err); }
    return res.json(200, exams);
  });
};

// Get list of exams per session
exports.sessionList = function(req, res) {
  Exam.find({ 'session': req.params.id},function (err, exams) {
    if(err) { return handleError(res, err); }
    return res.json(200, exams);
  });
};

// Get a single exam
exports.show = function(req, res) {
  Exam.findById(req.params.id, function (err, exam) {
    if(err) { return handleError(res, err); }
    if(!exam) { return res.send(404); }
    return res.json(exam);
  });
};

// Creates a new exam in the DB.
exports.create = function(req, res) {
   var examData=req.body;
   var examData2={};
   console.log(examData);
   ExamType.findOne({ 'name': req.body.type, 'method':req.body.method },'',function (err, examType) {
    if(err) { return handleError(res, err); }
    examData.report=examType.reportModel;
    examData.conclusion=examType.conclusionModel;
    examData2= examType
    console.log(examData2);
    Exam.create(examData, function(err, exam) {
    if(err) { return handleError(res, err); }
    return res.json(201, exam);
  });
    //return res.json(200, examType);
   });
};

// Get a single exam
exports.pdf = function(req, res) {

console.log(req.params.id)
var id=req.params.id;
http.createServer(function (req, res) {
  
  
  jsreport.render("<h1>Hello0000000000000000000 world</h1>").then(function(out) {
    
    setTimeout(function(){process.exit()},4000)
    out.result.pipe(res);

    //console.log(res)
  }).catch(function(e) {    
    res.end(e.message);
  });


}).listen(1337, '127.0.0.1');

return req;
};

// Updates an existing exam in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Exam.findById(req.params.id, function (err, exam) {
    if (err) { return handleError(res, err); }
    if(!exam) { return res.send(404); }
    var updated = _.merge(exam, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, exam);
    });
  });
};

// Deletes a exam from the DB.
exports.destroy = function(req, res) {
  Exam.findById(req.params.id, function (err, exam) {
    if(err) { return handleError(res, err); }
    if(!exam) { return res.send(404); }
    exam.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}

