'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var EquipmentSchema = new Schema({
  brand: String,
  name: String,
  year: Number
});

module.exports = mongoose.model('Equipment', EquipmentSchema);