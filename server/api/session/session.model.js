'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SessionSchema = new Schema({
  date: Date,
  room: String,
  method: String,
  sessionHead: String,
  sessionTeam:[],
  status: String
});

SessionSchema.pre('save', function(next) {
	//check if the function being saved does not already exist
	next();
});

module.exports = mongoose.model('Session', SessionSchema);