'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ExamTypeSchema = new Schema({
  name: String,
  method: String,
  techModel: String,
  reportModel: String,
  conclusionModel: String,
  active: Boolean
});

module.exports = mongoose.model('ExamType', ExamTypeSchema);