'use strict';

var _ = require('lodash');
var ExamType = require('./examType.model');

// Get list of examTypes
exports.index = function(req, res) {
  ExamType.find(function (err, examTypes) {
    if(err) { return handleError(res, err); }
    return res.json(200, examTypes);
  });
};

exports.filterByMethod = function(req, res) {
  console.log(req.query)
  ExamType.find({ 'method': req.query.method },function (err, examTypes) {
    if(err) { return handleError(res, err); }
    return res.json(200, examTypes);
  });
};

// Get a single examType
exports.show = function(req, res) {
  ExamType.findById(req.params.id, function (err, examType) {
    if(err) { return handleError(res, err); }
    if(!examType) { return res.send(404); }
    return res.json(examType);
  });
};

// Creates a new examType in the DB.
exports.create = function(req, res) {
  ExamType.create(req.body, function(err, examType) {
    if(err) { return handleError(res, err); }
    return res.json(201, examType);
  });
};

// Updates an existing examType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  ExamType.findById(req.params.id, function (err, examType) {
    if (err) { return handleError(res, err); }
    if(!examType) { return res.send(404); }
    var updated = _.merge(examType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.json(200, examType);
    });
  });
};

// Deletes a examType from the DB.
exports.destroy = function(req, res) {
  ExamType.findById(req.params.id, function (err, examType) {
    if(err) { return handleError(res, err); }
    if(!examType) { return res.send(404); }
    examType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}