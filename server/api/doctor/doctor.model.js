'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DoctorSchema = new Schema({
  name: String,
  rank: String,
  active: Boolean
});

module.exports = mongoose.model('Doctor', DoctorSchema);