/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var jsreport = require('jsreport');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/exams', require('./api/exam'));
  app.use('/api/sessions', require('./api/session'));
  app.use('/api/exam_types', require('./api/examType'));
  app.use('/api/methods', require('./api/method'));
  app.use('/api/rooms', require('./api/room'));
  app.use('/api/equipments', require('./api/equipment'));
  app.use('/api/doctors', require('./api/doctor'));
  app.use('/api/ranks', require('./api/rank'));
  app.use('/api/things', require('./api/thing'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));
  
  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

 
  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendfile(app.get('appPath') + '/index.html');
    });

  
};
